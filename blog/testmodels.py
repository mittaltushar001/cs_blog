from django.db import models

class Community(models.Model):
    name = models.CharField(max_length=20)

class VoteGiven(models.Model):
    user
    article
    vote_given

class Article(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    community = models.ForeignKey(community)
    article_text = models.TextField(max_length=200)
    votes = models.IntegerField(default=0)

class Comments(models.Model):
    comment_text = models.CharField(max_length=50)
    article = models.ForeignKey(article, on_delete=models.CASCADE)  

class Reply(models.Model):
    comment=
    user=
    reply=

* There are multiple Communities - Users can create create community
* A user can submit an article to a community
* A user comment on an article
* A user upvote / downvote an article    